<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8" />
    <title>Цены</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="css/jquery.bxslider.css">
    <link rel="stylesheet" href="css/uikit.css">
    <link rel="stylesheet" href="css/style.css">  
    <link rel="stylesheet" href="css/media.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
    <script type="text/javascript" src="js/jquery.bxslider.min.js"></script>
    <script type="text/javascript" src="js/uikit.min.js"></script>
    <script type="text/javascript" src="js/main.js"></script>
</head>

<body>
    <div class="ncf_container">
        <!--        шапка сайта начало-->
        <? require( 'header.html'); ?>
        <!--        шапка сайта конец-->

        <div class="ncf_aquamarine-title">
            Цены
            <div class="ncf_subtitle">Политика
                <br/> некафе</div>
        </div>

<!--прайс-лист начало-->
<!--       #PriceList.ncf_element-table>.ncf_element-table-item*8>#Definition$.ncf_element-table-item-container-->
        <div id="PriceList" class="ncf_element-table">
            <div class="ncf_element-table-item">
                <div class="ncf_element-table-item-container definition1">
                    Некафе - то самое место, где привычная жизнь выходит за рамки обыденного, это пространство, свободное для отдыха и работы, общения и уединения, творчества и развития. Мы создаём для вас идеальную атмосферу для отдыха и хобби, ведь время никогда не повернется вспять, но каждому хотелось бы проводить его с пользой и удовольствием. Для этого мы предоставляем вам наше СВОБОДНОЕ пространство Некафе.
                </div>
            </div>
            <div class="ncf_element-table-item">
                <div class="ncf_element-table-item-container definition2"></div>
            </div>
            <div class="ncf_element-table-item">
                <div class="ncf_element-table-item-container definition3"></div>
            </div>
            <div class="ncf_element-table-item">
                <div class="ncf_element-table-item-container definition4">
                    Вы платите только за время!<br/>
                    <div>
                        2руб./минута
                    </div>
                    <br/>
                    После 4 часов - <span>бесплатно</span>
                </div>
            </div>
            <div class="ncf_element-table-item">
                <div class="ncf_element-table-item-container definition5">
                    У нас для вас всегда есть вкусный заварной чай и бодрящий кофе. Наслаждайтесь тостами с джемом и многими другими разнообразными сладостями.
                    <p>
                        Вы можете приносить с собой любую еду, напитки, и всё, что душе угодно. Кроме того, мы полностью исключаем появление у нас сигарет и алкоголя в любом виде.
                    </p>
                </div>
            </div>
            <div class="ncf_element-table-item">
                <div class="ncf_element-table-item-container definition6"></div>
            </div>
            <div class="ncf_element-table-item">
                <div class="ncf_element-table-item-container definition7"></div>
            </div>
            <div class="ncf_element-table-item">
                <div class="ncf_element-table-item-container definition8">
                    Идея Некафе является катализатором яркой и продуктивной жизни творческих и интересных людей.
                    <p>Следите за событиями и новостями, и тогда вы сможете стать участником интересных встреч и мероприятий!</p>
                </div>
            </div>

        </div>
<!--        прайс-лист конец-->


        <!--    подвал начало-->
        <? require( 'footer.html'); ?>
        <!--    подвал конец-->

    </div>

    
</body>

</html>