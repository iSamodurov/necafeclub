"use strict";
/*
*переданные аргументы после второго будут функциями обратного вызова после отработки ajax запроса
*/
function checkHallForm(form, callback) {
    if ((form.mail.value == "") ||  (form.phone.value == "") || (form.hole.selectedIndex == 0))  {
        UIkit.modal.alert("Заполнены не все поля!");
    } else {       
        //отделяем ненужные параметры функции для передачи только callbacks
        var args = Array.prototype.slice.call(arguments);
        callback(form, args.slice(2,args.length),'hole-reserve.php');
              
    }
    
}

function feedbackCall(form, callback) {
    if ((form.mail.value == "") ||  (form.phone.value == ""))  {
        UIkit.modal.alert("Заполнены не все поля!");
    } else {       
        //отделяем ненужные параметры функции для передачи только callbacks
        var args = Array.prototype.slice.call(arguments);
        callback(form, args.slice(2,args.length),'feedback-call.php');
              
    }
}

/*
*переданные аргументы после первого будут функциями обратного вызова после отработки ajax запроса
*/
function call(form, callbacks,phpUrl) {
    var msg = $(form).serialize();
    $.ajax({
          type: 'POST',
          url: phpUrl,
          data: msg,
          success: function(data) {
            if(data){
                UIkit.modal.alert('Заявка успешно принята. Спасибо!');
                for (var i=0; i<callbacks.length; i++){
                    if(typeof(callbacks[i]) == "function"){
                        callbacks[i]();
                    }
                }
                form.reset();
//                for(var i = 0; i<form.length;i++){
//                    if(form[i].tagName == "INPUT"){
//                        form[i].value = "";
//                    }
//                    if(form[i].tagName == "SELECT"){
//                        form[i].selectedIndex = 0;
//                    }
//                }
            } else {
                UIkit.modal.alert('При обработке заявки возникла ошибка, попробуйте еще раз или позвоните нам по телефону.');
            }
          },
          error:  function(xhr, str){
                UIkit.modal.alert('Exception: ' + xhr.responseCode);
            }
        });
}

function selectHall(num){
    $("[name = 'send_request_dialog'] select")[0].selectedIndex = num;
}