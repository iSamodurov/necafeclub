<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8" />
    <title>Афиша</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="css/uikit.css">
    <link rel="stylesheet" href="css/components/slider.min.css">
    <link rel="stylesheet" href="css/components/slidenav.min.css">
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/media.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
    <script type="text/javascript" src="js/uikit.min.js"></script>
    <script type="text/javascript" src="js/components/slider.min.js"></script>
    <script type="text/javascript" src="js/main.js"></script>
</head>

<body>
    <div class="ncf_container">
        <!--        шапка сайта начало-->
        <? require( 'header.html'); ?>
        <!--        шапка сайта конец-->

        <div class="ncf_aquamarine-title">
            Афиша
            <div class="ncf_subtitle">Календарь
                <br/>событий</div>
        </div>

        <div class="ncf_schedule-area">
            <div class="ncf_schedule-notes">
                <h2>Режим работы Некафе:</h2>
                <div class="ncf_schedule-notes-lable">24/7</div>
                <p>С нашей ценовой политикой вы можете ознакомиться в разделе <a href="price.html">цены</a>
                </p>
                <p>Узнавайте обо всем первыми в разделе <a href="news.html">новости</a>, а также на наших страничках</p>
                <div class="ncf_social-networks">
                    <a class="vk-link" target="_blank" href="https://vk.com/necafeloft">
                    </a>
                    <a class="instagram-link" target="_blank" href="https://instagram.com/necafeloft/">
                    </a>

                </div>
            </div>

            <div class="ncf_calendar">
                <div class="ncf_calendar-container" id="calendar"></div>
            </div>

        </div>

        <div data-uk-slider class="ncf_posters uk-slidenav-position">

            <div class="uk-slider-container">
                <ul class="uk-slider uk-grid-width-medium-1-4">
                    <!--li*4>a>img[src='images/poster-slide-$.png']-->
                    <li>
                        <a href="https://vk.com/indianecafe" target="_blank"><img src="images/poster-slide-1.jpg" alt="">
                        </a>
                    </li>
                    <li>
                        <a href="#"><img src="images/poster-slide-2.jpg" alt="">
                        </a>
                    </li>
                    <li>
                        <a href="https://vk.com/hnanecafe" target="_blank"><img src="images/poster-slide-3.jpg" alt="">
                        </a>
                    </li>
                    <li>
                        <a href="https://vk.com/englishnecafe" target="_blank"><img src="images/poster-slide-4.jpg" alt="">
                        </a>
                    </li>
                </ul>
            </div>
            <a href="" class="uk-slidenav uk-slidenav-contrast uk-slidenav-previous" data-uk-slider-item="previous"></a>
            <a href="" class="uk-slidenav uk-slidenav-contrast uk-slidenav-next" data-uk-slider-item="next"></a>


        </div>

        <!--    подвал начало-->
        <? require( 'footer.html'); ?>
        <!--    подвал конец-->

    </div>
    <!--
    <script>
        var groupId = '-53878218';
        var param = {
            owner_id: groupId,
            count: 1,
            callback: 'callbackFunc'
        };

        //    \|[^]*?\[
        //    \[.+\]

        var script = document.createElement('SCRIPT');
        script.src = "https://api.vk.com/method/wall.get?";
        for (var key in param) {
            script.src += key + "=" + param[key] + "&";
        }

        document.getElementsByTagName("head")[0].appendChild(script);

        function callbackFunc(result) {
            console.log(result);
            var oWallItem = result.response[1].text;
            var r = RegExp(/\[[^]+?\]/g);
            var arrPosts = oWallItem.match(r);
            console.log(arrPosts);
        }
    </script>-->
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/fullcalendar/2.3.2/fullcalendar.min.css">
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/fullcalendar/2.3.2/fullcalendar.print.css" media="print">
    <script src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.3/moment.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/fullcalendar/2.3.2/fullcalendar.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/fullcalendar/2.3.2/lang-all.js"></script>
    <script>
        $(document).ready(function () {
            $('#calendar').fullCalendar({
                lang: 'ru',
                header: {
                    left: 'prev,next today',
                    center: 'title',
                    right: 'month,agendaWeek,agendaDay'
                },
                buttonIcons: false, // show the prev/next text
                weekNumbers: true,
                editable: true,
                eventLimit: true, // allow "more" link when too many events
                eventColor: '#ccc',
                events: [
                    {
                        title: 'All Day Event',
                        start: '2015-06-20'
     },
                    {
                        title: 'Long Event',
                        start: '2015-06-18',
                        end: '2015-06-17'
     }
    ]
            });
        });
    </script>
</body>

</html>