<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Главная</title>
    <link rel="stylesheet" href="css/jquery.bxslider.css">
    <link rel="stylesheet" href="css/uikit.css">
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/media.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
    <script type="text/javascript" src="js/jquery.bxslider.min.js"></script>
    <script type="text/javascript" src="js/uikit.min.js"></script>
    <script type="text/javascript" src="js/main.js"></script>
    <script src="//api-maps.yandex.ru/2.0/?load=package.standard&lang=ru-RU" type="text/javascript"></script>
</head>

<body>
    <div class="ncf_container">
        <!--        шапка сайта начало-->
        <? require( 'header.html'); ?>
        <!--        шапка сайта конец-->
        <!--       слайдер начало-->
        <div class="ncf_slider ncf_main-page-slider">
            <ul class="bxslider">
                <li>
                    <div class="ncf_slider-item">
                        <img src="images/slider-pict1.jpg" alt="">
                        <div class="ncf_slider-item-info">
                            <div class="ncf_slider-item-info-title">Вы платите только за время!</div>
                            <div class="ncf_slider-item-info-text">Вкусный чай, бодрящий кофе, тосты с джемом, печенье и другие сладости. Занимательные настольные игры, XBOX и еще множество развлечений помогут весело и с пользой провести время в компании друзей. А быстрый интернет и тишина переговорной комнаты располагают к плодотворной работе и деловым встречам.</div>
                        </div>
                    </div>
                </li>
                <li>
                    <div class="ncf_slider-item">
                        <img src="images/slider-pict1.jpg" alt="">
                        <div class="ncf_slider-item-info">
                            <div class="ncf_slider-item-info-title">Вы платите только за время!</div>
                            <div class="ncf_slider-item-info-text">Вкусный чай, бодрящий кофе, тосты с джемом, печенье и другие сладости. Занимательные настольные игры, XBOX и еще множество развлечений помогут весело и с пользой провести время в компании друзей. А быстрый интернет и тишина переговорной комнаты располагают к плодотворной работе и деловым встречам.</div>
                        </div>
                    </div>
                </li>
            </ul>
        </div>
        <!--       слайдер конец-->

        <!--    меню начало-->
        <div id="MainPageMenu" class="ncf_element-table">
            <div class="ncf_element-table-item">
                <a href="poster.php" id="Poster" class="ncf_element-table-item-container">
                    <img src="images/poster-logo.png" alt="">
                    <h1>Афиша</h1>
                    <div class="ncf_menu-item-description">Календарь событий</div>
                </a>
            </div>
            <div class="ncf_element-table-item">
                <a href="news.php" id="News" class="ncf_element-table-item-container">
                    <h1>Новости</h1>
                    <div class="ncf_menu-item-description">Фото, видео и много интересной информации</div>
                </a>
            </div>
            <div class="ncf_element-table-item">
                <a href="halls.php" id="Halls" class="ncf_element-table-item-container">
                    <h1>Залы</h1>
                    <div class="ncf_menu-item-description">Бар, переговорная, центральная комната, Chillout, kinect, танцпол, пуэр-бар, XBOX, амфитеатр, 2 этаж</div>
                </a>
            </div>
            <div class="ncf_element-table-item">
                <a href="price.php" id="Price" class="ncf_element-table-item-container">
                    <img src="images/price-logo.png" alt="">
                    <h1>Цены</h1>
                    <div class="ncf_menu-item-description">Политика некафе</div>
                </a>
            </div>
            <div class="ncf_element-table-item">
                <a href="tour.php" id="Tour3D" class="ncf_element-table-item-container">
                    <img src="images/3d-tour-logo.png" alt="">
                    <h1>3D тур</h1>
                    <div class="ncf_menu-item-description">Путешествуйте по нашим залам</div>
                </a>
            </div>
            <div class="ncf_element-table-item">
               <div class="vk-container">
                <a href="https://vk.com/necafeloft" target="_blank">
                    <img src="images/vk-logo.svg" alt="">
                </a>
               </div>
               
               <div class="instagram-container">
                   <a href="https://instagram.com/necafeloft/">
                    <img src="images/instagram-logo.svg" alt="">
				   </a>
               <!-- INSTANSIVE WIDGET -->
               <script src="//instansive.com/widget/js/instansive.js"></script><iframe src="//instansive.com/widgets/5ad90bc28b3d772f270e6ff0ee6579a9650662c3.html" id="instansive_5ad90bc28b" name="instansive_5ad90bc28b"  scrolling="no" allowtransparency="true" class="instansive-widget" style="width: 100%; border: 0; overflow: hidden;"></iframe>
               </div>
            </div>
            <div class="ncf_element-table-item">
                <div id="Contacts" class="ncf_element-table-item-container">
                    <h1>Контакты</h1>
                    <div class="ncf_menu-item-contacts-address">ул.Большая Московская, д.11</div>
                    <div class="ncf_menu-item-contacts-phone">8 (904) 958 96 95</div>
                    <div class="ncf_order-hall-btn">
                        <button data-uk-modal="{target:'#hole-dialog'}">Заказать зал</button>
                    </div>
                </div>
            </div>
            <div class="ncf_element-table-item">
                <div id="YMapContainer" class="ncf_element-table-item-container">
                    <script type="text/javascript">
                        function fid_64584462(ymaps) {
                            var objects = [];
                            var events = {};
                            try {} catch (e) {
                                alert(e);
                            };
                            var map;
                            objects["map1"] = map = new ymaps.Map("YMapContainer", {
                                center: [56.127662775672704, 40.40009499999995],
                                zoom: 18,
                                type: "yandex#map",
                                behaviors: ['drag', 'dblClickZoom']
                            });
                            map.controls.add("typeSelector", {
                                "top": -62.5,
                                "right": 10
                            }).add("zoomControl", {
                                "top": 30,
                                "left": 10
                            });
                            map.geoObjects.add(objects['Point4'] = new ymaps.Placemark([56.12769723065869, 40.40006013128275], {
                                "iconContent": "",
                                "balloonContent": "Большая Московская улица, 11",
                                //"hintContent": "Большая Московская улица, 11",
                                "xname": "Point4",
                                "metaType": "Point"
                            }, {
                                "preset": "twirl#darkorangeDotIcon",
                                "visible": true,
                                "iconImageHref": "https://api-maps.yandex.ru/2.0.41/images/88b3e0581a764c12e576db60d7c1c78e.png",
                                "iconImageSize": [37, 42],
                                "iconImageOffset": [-10, -40],
                                "iconContentLayout": "twirl#geoObjectIconContent",
                                "balloonContentBodyLayout": "twirl#geoObjectBalloonBodyContent",
                                "hintContentLayout": "twirl#geoObjectHintContent"
                            }));
                        };
                    </script>
                    <script type="text/javascript" src="http://api-maps.yandex.ru/2.0-stable/?load=package.full&lang=ru-RU&onload=fid_64584462"></script>
                    <!-- Этот блок кода нужно вставить в ту часть страницы, где вы хотите разместить карту (конец) -->

                </div>
            </div>
        </div>
        <!--    меню конец-->

        <!--    подвал начало-->
        <? require( 'footer.html'); ?>
        <!--    подвал конец-->

    </div>


    <script>
        jQuery('.bxslider').bxSlider({controls : false, auto:true, pause:3000});
        $(document).ready(function () {
            $('.ncf_order-hall-btn button').click(function (e) {
                e.preventDefault();
            });
        });
    </script>
</body>

</html>