<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8" />
    <title>3D тур</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="css/uikit.css">
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/media.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
    <script type="text/javascript" src="http://assets5.navse360.ru/js/embedpano.js?v=1.0.26"></script>
    <script type="text/javascript" src="js/uikit.min.js"></script>
    <script type="text/javascript" src="js/main.js"></script>
</head>

<body>
    <div class="ncf_container">
        <!--        шапка сайта начало-->
        <? require( 'header.html'); ?>
        <!--        шапка сайта конец-->

        <div class="ncf_aquamarine-title">
            3D тур
            <div class="ncf_subtitle">Путешествуй
                <br/>по нашим залам</div>
        </div>

        <div class="ncf_tour-area">
            <div class="ncf_tour-container" id="krpano"></div>
        </div>

        <!--    подвал начало-->
        <? require( 'footer.html'); ?>
        <!--    подвал конец-->

    </div>

    <script type="text/javascript">
        $(document).ready(function () {
            embedpano({
                target: 'krpano',
                swf: 'http://vladimir.navse360.ru/tours/4621/pano/tour.swf',
                height: '100%',
                width: '100%',
                wmode: 'opaque',
                html5: 'fallback',
                passQueryParameters: true
            });
        });
    </script>

</body>

</html>