<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8" />
    <title>Залы</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="css/jquery.bxslider.css">
    <link rel="stylesheet" href="css/uikit.css">
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/media.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
    <script type="text/javascript" src="js/uikit.min.js"></script>
    <script type="text/javascript" src="js/jquery.bxslider.min.js"></script>
    <script type="text/javascript" src="js/main.js"></script>
</head>

<body>
    <div class="ncf_container">
        <!--        шапка сайта начало-->
        <? require( 'header.html'); ?>
        <!--        шапка сайта конец-->

        <div class="ncf_bg-title  halls-title">
            Залы
            <div class="ncf_subtitle">БАР, ПЕРЕГОВОРНАЯ, ЦЕНТРАЛЬНАЯ КОМНАТА, CHILLOUT,
                <br/>KINECT, ТАНЦПОЛ, ПУЭР-БАР, XBOX, АМФИТЕАТР, 2 ЭТАЖ
            </div>
        </div>

        <div class="ncf_page-description">
            Далеко-далеко за словесными горами в стране гласных и согласных живут рыбные тексты. Вдали от всех живут они в буквенных домах на берегу Семантика большого языкового океана. Маленький ручеек Даль журчит по всей стране и обеспечивает ее всеми необходимыми правилами. Эта парадигматическая страна, в которой жаренные члены предложения залетают прямо в рот!
        </div>

        <!--список залов начало-->
        <!--       #PriceList.ncf_element-table>.ncf_element-table-item*8>#Definition$.ncf_element-table-item-container-->
        <div class="ncf_element-table halls-list">
            <div class="ncf_element-table-item">
                <div class="ncf_element-table-item-container definition1">
                    <div class="ncf_element-table-item-content">
                        <h3>Бар</h3>
                        <div class="ncf_hole-desc">
                            <p>Бесплатный: черный/зеленый чай (самообслуживание), печенье/сладости, кофе (американо, капучино, эспрессо), лимонад, какао.
                            </p>
                            <p>Платный: фирменный кофе (гляссе (кофе+мороженое), латте (кофе+сироп)), американо-капучино-эспрессо с сиропом, соки в ассортименте.</p>
                            <p>Можно приносить еду с собой.</p>
                        </div>
                        <div class="ncf_order-hall-btn">
                            <button onclick="selectHall(1)" data-uk-modal="{target:'#hole-dialog'}">Заказать зал</button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="ncf_element-table-item">
                <div class="ncf_element-table-item-container definition2">
                    <div class="ncf_element-table-item-content">
                        <h3>Переговорная</h3>
                        <div class="ncf_hole-desc">
                            <p>Стеклянная комната со звукоизоляцией, внутри поддерживается "приятная прохлада" для активной работы.</p>
                            <p>Можно забронировать для коворкинга, репетиции, просмотра кино или дня рождения. Есть проектор.</p>
                            <div class="ncf_order-hall-btn">
                                <button onclick="selectHall(2)" data-uk-modal="{target:'#hole-dialog'}">Заказать зал</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="ncf_element-table-item">
                <div class="ncf_element-table-item-container definition3">
                    <div class="ncf_element-table-item-content">
                        <h3>Центральная комната</h3>
                        <div class="ncf_hole-desc">
                            <p>Предназначена для активных игр (при необходимости ставим настольный теннис), йоги, танцев (хореографии), мини-экспозиций, презентаций, закрытых мероприятий, можно арендовать для просмотра фильмов, фотостудии, репетиций, "свиданий вслепую". Хорошая вентиляция, можно полностью выключить свет.</p>
                            <div class="ncf_order-hall-btn">
                                <button onclick="selectHall(3)" data-uk-modal="{target:'#hole-dialog'}">Заказать зал</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="ncf_element-table-item">
                <div class="ncf_element-table-item-container definition4">
                    <div class="ncf_element-table-item-content">
                        <h3>Chillout</h3>
                        <div class="ncf_hole-desc">
                            <p>Отдых, уют, удобно играть в настольные игры, проводить время с друзьями.</p>
                            <div class="ncf_order-hall-btn">
                                <button onclick="selectHall(4)" data-uk-modal="{target:'#hole-dialog'}">Заказать зал</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="ncf_element-table-item">
                <div class="ncf_element-table-item-container definition5">
                    <div class="ncf_element-table-item-content">
                        <h3>Kinect</h3>
                        <div class="ncf_hole-desc">
                            <p>Подходит для отдыха компанией, можно играть в Xbox (есть Kinect и соответствующие игры), танцевать.</p>
                            <div class="ncf_order-hall-btn">
                                <button onclick="selectHall(5)" data-uk-modal="{target:'#hole-dialog'}">Заказать зал</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="ncf_element-table-item">
                <div class="ncf_element-table-item-container definition6">
                    <div class="ncf_element-table-item-content">
                        <h3>Танцпол</h3>
                        <div class="ncf_hole-desc">
                            <p>Просторная зона, где расположен уголок ретро-приставок (Sega, Dendy) с 1000 игр, шоу-рум (можно арендовать место под собственную продажу), танцпол (персонально выставляется оборудование для выступлений).</p>
                            <div class="ncf_order-hall-btn">
                                <button onclick="selectHall(6)" data-uk-modal="{target:'#hole-dialog'}">Заказать зал</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="ncf_element-table-item">
                <div class="ncf_element-table-item-container definition7">
                    <div class="ncf_element-table-item-content">
                        <h3>Пуэр-бар</h3>
                        <div class="ncf_hole-desc">
                            <p>Каждый вечер - большой выбор настоящего китайского чая (пуэр, улун, зелёный, красный, всегда разные сорта). Чайные церемонии, истории, лекции. Можной прийти компанией и занять беседку на весь вечер при договорённости с чайным мастером.</p>
                            <div class="ncf_order-hall-btn">
                                <button onclick="selectHall(7)" data-uk-modal="{target:'#hole-dialog'}">Заказать зал</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="ncf_element-table-item">
                <div class="ncf_element-table-item-container definition8">
                    <div class="ncf_element-table-item-content">
                        <h3>Xbox</h3>
                        <div class="ncf_hole-desc">
                            <p>4 приставки Xbox 360 с большим выбором игр, мягкие посадочные места. Игры объединены в локальную сеть, можно прийти компанией и устроить турнир по Halo, Mortal Kombat, Left4Dead 2, Injustice, Call of Duty и т.п.</p>
                            <div class="ncf_order-hall-btn">
                                <button onclick="selectHall(8)" data-uk-modal="{target:'#hole-dialog'}">Заказать зал</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="ncf_element-table-item">
                <div class="ncf_element-table-item-container definition9">
                    <div class="ncf_element-table-item-content">
                        <h3>Амфитеатр</h3>
                        <div class="ncf_hole-desc">
                            <p>Можно арендовать для проведения лекций, презентаций, концертов, собраний, детских утренников, просмотра фильмов.</p>
                            <div class="ncf_order-hall-btn">
                                <button onclick="selectHall(9)" data-uk-modal="{target:'#hole-dialog'}">Заказать зал</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="ncf_element-table-item">
                <div class="ncf_element-table-item-container definition10">
                    <div class="ncf_element-table-item-content">
                        <h3>2 этаж</h3>
                        <div class="ncf_hole-desc">
                            <p>Зона со стеклянным потолком и телескопом, арендуется под фотосессии, встречи, дни рождения, мастер-классы, удобно для проведения мероприятий, где требуются столы. Можно по периметру повесить фотографии или панно (экспозиции).</p>
                            <div class="ncf_order-hall-btn">
                                <button onclick="selectHall(10)" data-uk-modal="{target:'#hole-dialog'}">Заказать зал</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <!-- список залов конец-->


        <!--    подвал начало-->
        <? require( 'footer.html'); ?>
        <!--    подвал конец-->

    </div>
</body>

</html>