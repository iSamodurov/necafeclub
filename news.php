<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8" />
    <title>Новости</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="css/uikit.css">
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/media.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
    <script type="text/javascript" src="js/uikit.min.js"></script>
    <script type="text/javascript" src="js/main.js"></script>
</head>

<body>
    <div class="ncf_container">
        <!--        шапка сайта начало-->
        <? require( 'header.html'); ?>
        <!--        шапка сайта конец-->

        <div class="ncf_bg-title news-title">
            Новости
            <div class="ncf_subtitle"> Фото, видео и много интересной
                <br/>информации
            </div>
        </div>

        <div class="ncf_news-content">
            <div class="ncf_news-content-cell ncf_news-area">
                <h2>Новости</h2>
                <div class="ncf_news-list" id="NewsList">
                    <!--
                        <div class="ncf_news-list-item">
                            <div class="ncf_news-item-date">
                                5 мая, 2015г
                            </div>
                            <div class="ncf_news-item-img"></div>
                            <p>Задача организации, в особенности же сложившаяся структура организации требуют определения и уточнения форм развития. Товарищи! реализация намеченных плановых заданий в значительной степени обуславливает создание форм развития. Разнообразный и богатый опыт постоянное информационно-пропагандистское обеспечение нашей деятельности обеспечивает широкому кругу (специалистов) участие в формировании направлений прогрессивного развития. Повседневная практика показывает, что новая модель организационной деятельности позволяет выполнять важные задания по разработке соответствующий условий активизации. С другой стороны постоянное информационно-пропагандистское обеспечение нашей деятельности представляет собой интересный эксперимент проверки дальнейших направлений развития.</p>
                        </div>
-->
                </div>
                <div class="ncf_news-archive"> <a href="#">Архив новостей</a>
                </div>
            </div>
            <div class="ncf_news-content-cell ncf_media-area">
                <h2>Фото</h2>
                <div class="ncf_photo-area ncf_element-table" id="Photos">
                    <!--
                        <div class="ncf_element-table-item">
                            <div class="ncf_element-table-item-container" onmouseover="albumOver(this, '-53878218_202652807');" onmouseout="albumOut(this, '-53878218_202652807', true);">
                                <a href="/album-53878218_202652807" class="img_link">
                                    <img src="https://pp.vk.me/c619226/v619226189/17a45/yu3Zd3hRNJI.jpg" />
                                    <div class="page_album_title_wrap">
                                        <div class="clear_fix" style="margin: 0px">
                                            <div class="page_album_title" title="Пижамапати&#33;">Пижамапати&#33;</div>
                                            <div class="page_album_camera">29</div>
                                        </div>
                                        <div class="page_album_description">29 ноября-2 декабря в Некафе состоялся турнир по Монополии, организованный совместно с федеральной программой \"Ты-предприниматель\". Победителем стал Алексей Есин, который выиграл iPad-mini.</div>
                                    </div>
                                </a>
                            </div>
                        </div>
-->
                </div>
                <h2>Видео</h2>
                <div class="ncf_video-area ncf_element-table" id="Videos">
                    <div class="ncf_element-table-item">
                        <div class="ncf_element-table-item-container">
                            <iframe src="https://www.youtube.com/embed/2E72Mwf1Qrs" frameborder="0" allowfullscreen></iframe>
                        </div>
                    </div>
                    <div class="ncf_element-table-item">
                        <div class="ncf_element-table-item-container">
                            <iframe src="https://www.youtube.com/embed/P2QdN6D-Zyc" frameborder="0" allowfullscreen></iframe>
                        </div>
                    </div>
                    <div class="ncf_element-table-item">
                        <div class="ncf_element-table-item-container">
                            <iframe src="https://www.youtube.com/embed/7KvzX4bx5p0" frameborder="0" allowfullscreen></iframe>
                        </div>
                    </div>
                    <div class="ncf_element-table-item">
                        <div class="ncf_element-table-item-container">
                            <iframe src="https://www.youtube.com/embed/wYKkpe5xq28" frameborder="0" allowfullscreen></iframe>
                        </div>
                    </div>
                </div>
            </div>

        </div>


        <!--    подвал начало-->
        <? require( 'footer.html'); ?>
        <!--    подвал конец-->

    </div>
    <script>
        initPhotoAlbums();
        initNews();

        function initPhotoAlbums() {
            var groupId = '-53878218';
            var script = document.createElement('SCRIPT');
            var param = {
                owner_id: groupId,
                count: 6,
                need_covers: 1,
                photo_sizes: 1,
                callback: 'callbackFuncAlbums'
            };
            script.src = "https://api.vk.com/method/photos.getAlbums?";

            for (var key in param) {
                script.src += key + "=" + param[key] + "&";
            }

            document.getElementsByTagName("head")[0].appendChild(script);
        }

        function initNews() {
            var groupId = '-53878218';
            var script = document.createElement('SCRIPT');
            var param = {
                owner_id: groupId,
                count: 5,
                callback: 'callbackFuncNews'
            };
            script.src = "https://api.vk.com/method/wall.get?";

            for (var key in param) {
                script.src += key + "=" + param[key] + "&";
            }

            document.getElementsByTagName("head")[0].appendChild(script);
        }


        function callbackFuncAlbums(result) {
            var album = "Photos";
            result.response.forEach(function (item) {
                var htmlItem = '';
                var container = 'ncf_element-table-item-container';
                var img;
                var containerSize = {
                    width: 277,
                    height: 185
                };

                for (var i = 0; i < item.sizes.length; i++) {
                    if ((item.sizes[i].width >= containerSize.width) && (item.sizes[i].height >= containerSize.height)) {
                        img = item.sizes[i].src;
                        break;
                    }
                }


                htmlItem = '<div class="ncf_element-table-item">' +
                    '<div class="' + container + '" onmouseover="albumOver(this, \'-53878218_202652807\');" onmouseout="albumOut(this, \'-53878218_202652807\', true);">' +
                    '<a href="https://vk.com/album-53878218_' + item.aid + '" class="img_link">' +
                    '<div class="album-bg" style="background-image: url(' + img + ');" />' +
                    '<div class="page_album_title_wrap">' +
                    '<div class="clear_fix" style="margin: 0px">' +
                    '<div class="page_album_title" title="' + item.title + '">' + item.title + '</div>' +
                    '<div class="page_album_camera">' + item.size + '</div>' +
                    '</div>' +
                    '<div class="page_album_description">' + item.description + '</div>' +
                    '</div>' +
                    '</a>' +
                    '</div>' +
                    '</div>';
                $('#' + album).append(htmlItem);
            });
        }


        function callbackFuncNews(result) {

            var album = "NewsList";
            var img;
            var htmlItem = '';
            var newsText;
            for (var i = 2; i < result.response.length; i++) {
                img = ((result.response[i].attachment) && (result.response[i].attachment.type == "photo")) ? '<div class="ncf_news-item-img" style="background-image:url(' + result.response[i].attachment.photo.src_big + ');"></div>' : '';
                newsText = GetText(result.response[i].text);



                htmlItem = '<div class="ncf_news-list-item">' +
                    '<div class="ncf_news-item-date">' + getDateString(result.response[i].date * 1000) + '</div>' +
                    img +
                    '<p>' + newsText + '</p>' +
                    '</div>';

                $('#' + album).append(htmlItem);
            }
        }


        function GetText(AInputText) {
            // AInputText - входной текс 
            // VRegExp - экземпляр объекта RegExp (регулярное выражение)
            // VResult - результат полученный после применения регулярного выражения 
            var VResult;
            var replacer;
            var VRegExp = new RegExp(/\[[^]+?\]/g);
            var arrMatch = AInputText.match(VRegExp) || [];
            VResult = AInputText;

            arrMatch.forEach(function (item, i, arr) {
                replacer = GetLink(item);
                VResult = VResult.replace(item, replacer);
            });

            return VResult;
        }

        function GetLink(AInputText) {
            var VResult;
            var buff = AInputText.replace(new RegExp(/[\[,\]]/g), "");
            buff = buff.split('|');
            VResult = '<a href="https://vk.com/' + buff[0] + '" class="news-link" target="_blank">' + buff[1] + '</a>';
            return VResult;
        }

        function getDateString(iCount) {
            var aMonths = ['Января', 'Февраля', 'Марта', 'Апреля', 'Мая', 'Июня', 'Июля', 'Августа', 'Сентября', 'Октября', 'Ноября', 'Декабря'];
            var dDate = new Date(iCount);
            return dDate.getDate() + " " + aMonths[dDate.getMonth()] + ", " + dDate.getFullYear() + "г";
        }

        function albumOver(obj, id) {
            obj.getElementsByClassName('page_album_title_wrap');
            obj.getElementsByClassName('page_album_description');
        }

        function albumOut(obj, id) {}

        //        $.ajax({
        //            type: 'get',
        //            url: 'https://api.vk.com/method/wall.get',
        //            data: param,
        //            success: function (data) {
        //                console.log(data);
        //            }
        //        });
    </script>
</body>

</html>